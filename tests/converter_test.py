from tkshard.converters import converter_data

def test():
    md_converter = converter_data(".md")
    assert(md_converter.name == "Markdown")
    def_converter = converter_data(".xxx")
    assert(def_converter.name == "Markdown")
    rst_converter = converter_data(".rst")
    assert(rst_converter.name == "reStructured Text")