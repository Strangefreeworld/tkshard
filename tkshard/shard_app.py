import os
import tkinter as tk
from tkinter import filedialog, messagebox as mbox
#import ttkbootstrap as ttk
from loguru import logger
from tkshard import parent_dir
from app_config import CONFIG_FILE
from window_geometry import WindowGeometry, window_geometry
from editor_pane import EditorPane


class ShardApp(tk.Tk):
    def __init__(self):
        super().__init__()
        self.title("Shard")
        self.geometry("700x700")
        self._mainmenu = tk.Menu(self)
        app_image = tk.PhotoImage(file=os.path.join(parent_dir(), "shard.png"))
        self.iconphoto(False, app_image)
        filemenu = tk.Menu(self._mainmenu)
        filemenu.add_command(label="Open", command=self.openfile, accelerator="Ctrl+O")
        filemenu.add_command(label="Save", command=self.savefile, accelerator="Ctrl+S")
        filemenu.add_command(label="Save as", command=self.saveas)
        filemenu.add_separator()
        filemenu.add_command(label="Exit", command=self.exit)
        self.protocol("WM_DELETE_WINDOW", self.exit)
        self._mainmenu.add_cascade(label="File", menu=filemenu)
        editmenu = tk.Menu(self._mainmenu)
        editmenu.add_command(label="Undo", command=self.undo, accelerator="Ctrl+Z")
        editmenu.add_separator()
        editmenu.add_command(label="Cut", command=self.cut, accelerator="Ctrl+X")
        editmenu.add_command(label="Copy", command=self.copy, accelerator="Ctrl+C")
        editmenu.add_command(label="Paste", command=self.paste, accelerator="Ctrl+V")
        editmenu.add_command(label="Select All",
                             command=self.select_all,
                             accelerator="Ctrl+A")
        self._mainmenu.add_cascade(label="Edit", menu=editmenu)
        self.config(menu=self._mainmenu)
        self._editor = EditorPane(self)
        self._editor.pack(fill=tk.BOTH, expand=True)
        logger.info(CONFIG_FILE)
        self.after(300, self._update)
        self._last_hash = None

    def _geometry(self, window=None) -> WindowGeometry:
        check_window = self if window is None else window
        self.update_idletasks()
        return window_geometry(check_window)

    def _update(self):
        self._editor.update()
        self.after(300, self._update)

    def openfile(self):
        if self._editor.modified:
            answer = mbox.askquestion(
                "Warning", "Text has been modified. Do you want to save changes?"
            )
            if answer == "yes":
                self.savefile()

        openfilename = filedialog.askopenfilename(
            filetypes=(
                ("Markdown File", "*.md , *.mdown , *.markdown"),
                ("Text File", "*.txt"),
                ("All Files", "*.*"),
            )
        )
        if openfilename:
            self._editor.open_file(openfilename)

    def saveas(self):
        savefilename = filedialog.asksaveasfilename(
            filetypes=(("Markdown File", "*.md"), ("Text File", "*.txt")),
            title="Save Markdown File",
        )
        if savefilename:
            self._editor.save_as(savefilename)

    def savefile(self):
        if self._editor.filename is None:
            self.saveas()
        else:
            self._editor.save()

    def cut(self, _):
        self._editor.cut()

    def copy(self, _):
        self._editor.copy()

    def paste(self, _):
        self._editor.paste()

    def exit(self):
        if self._editor.modified:
            answer = mbox.askquestion(
                "Warning", "Text has been modified. Do you want to save changes?"
            )
            if answer == "yes":
                self.savefile()
        self.quit()

    def undo(self, _):
        self._editor.undo()

    def select_all(self, _):
        self._editor.select_all()