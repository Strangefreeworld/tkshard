import tkinter as tk
from tkinter import messagebox as mbox
from pathlib import Path
import hashlib
import ttkbootstrap as ttk
# from loguru import logger
from tkinterweb import HtmlFrame
from converters import converter_data
from text_frame import TextFrame

class EditorPane(ttk.PanedWindow):
    def __init__(self, parent):
        super().__init__(parent, orient=tk.HORIZONTAL)
        self._filename = None
        self._parent = parent
        self._text = TextFrame(self) #ttk.Text(self, undo=True)
        self._text.pack(fill=tk.BOTH, expand=True, side=tk.LEFT)
        self._preview = HtmlFrame(self)
        self._preview.pack(side=tk.RIGHT, fill=tk.BOTH, expand=True)
        self.add(self._text, weight=1)
        self.add(self._preview, weight=1)
        self._converter = converter_data("")
        self._last_save = self._hash_text()
        self._last_render = self._hash_text()

    def open_file(self, openfilename: str):
            with open(openfilename, encoding="utf-8") as open_file:
                self._text.editor.delete(1.0, tk.END)
                self._text.editor.insert(tk.END, open_file.read())
                self._filename = openfilename
                self._last_save = self._hash_text()
                self._converter = converter_data(Path(openfilename).suffix)

    def _filesave(self):
        try:
            with open(self._filename, "w") as f:
                filedata = self._text.editor.get("1.0", tk.END)
                f.write(filedata)
                self._last_save = self._hash_text()
        except Exception as e:
            mbox.showerror(
                "Error Saving File",
                f"The file {self._filename} can't be saved {str(e)}"
            )
    
    def save_as(self, filename):
        self._filename = filename
        self._filesave()

    def save(self):
        if not self._filename:
            raise FileNotFoundError
        self._filesave()

    def update(self):
        current_text = self._hash_text()
        if current_text != self._last_render:
            html_text = self._converter.function(self._text.editor.get("1.0", tk.END))
            self._preview.load_html(html_text)
            self._last_render = current_text

    @property
    def filename(self) -> str:
         return self._filename
    
    @property
    def modified(self) -> bool:
         return True if self._hash_text() != self._last_save else False

    def _hash_text(self) -> str:
        hasher = hashlib.new("sha1")
        hasher.update(self._text.editor.get("1.0", "end").encode("utf-8"))
        return hasher.hexdigest()

    def cut(self):
        self._text.event_generate("<<Cut>>")

    def copy(self):
        self._text.event_generate("<<Copy>>")

    def paste(self):
        self._text.event_generate("<<Paste>>")

    def select_all(self):
        self._text.tag_add(tk.SEL, "1.0", tk.END)
        self._text.mark_set(tk.INSERT, "1.0")
        self._text.see(tk.INSERT)
        return "break"

    def undo(self):
        self._text.edit_undo()
