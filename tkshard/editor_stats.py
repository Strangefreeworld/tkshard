from dataclasses import dataclass

@dataclass
class EditorPosition:
    line: int
    column: int

    def __init__(self, line, column):
        self.line = line
        self.column = column

@dataclass
class EditorStats:
    line_stats: EditorPosition
    number_lines: int
    character_count: int