# from collections import namedtuple
from dataclasses import dataclass
from collections.abc import Callable
import mistune
import textile
from docutils.core import publish_string
from mau import Mau

# Converter = namedtuple("Converter", "name function")

@dataclass(repr=True)
class Converter:
    name: str
    function: Callable[[str], str]

    def __init__(self, name: str, convert: Callable):
        self.name = name
        self.function = convert


def textile_convert(text: str) -> str:
    """This function converts Textile text into HTML"""
    return textile.textile(text)


def rst_convert(text: str) -> str:
    """This function converts reStructuredText into HTML"""
    return publish_string(text, writer_name="html")


def simple_markdown(text: str) -> str:
    return mistune.html(text)


class MauConverter:
    def __init__(self):
        self._mau = Mau({}, "html")

    def __call__(self, text: str) -> str:
        return self._mau.process(text)


CONVERTERS = [
    {"name": "Markdown", "extensions": [".md"], "converter": simple_markdown},
    {"name": "Textile", "extensions": [".textile"], "converter": textile_convert},
    {"name": "reStructured Text", "extensions": [".rst"], "converter": rst_convert},
    {"name": "Mau", "extensions": [".mau"], "converter": MauConverter()},
]


def get_converter(extension: str):
    for item in CONVERTERS:
        if extension in item["extensions"]:
            return Converter(item["name"], item["converter"])
        #Converter(item["name"], item["converter"])
    return None


def converter_data(extension: str) -> Converter:
    data = get_converter(extension)
    return data if data else get_converter(".md")


def get_function(text_format):
    for item in CONVERTERS:
        if item["name"] == text_format:
            return item["converter"]
    return None


def converter_function(text_format=None):
    function = get_function(text_format)
    return function if function else get_function("Markdown")
