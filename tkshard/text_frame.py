#import tkinter as tk
import ttkbootstrap as ttk
from chlorophyll import CodeView

class TextFrame(ttk.Frame):
    def __init__(self, parent):
        super().__init__(parent)
        self._edit = CodeView(self, undo=True, color_scheme="ayu-light")
        self._edit.pack(fill="both", expand=True)

    @property
    def editor(self):
        return self._edit
